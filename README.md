*** TO RUN THE FRONTEND ***
Look in the package.json file for react scripts commands
- yarn start or expo start

Once it opens, you can either use android studio or xcode to run emulators
or you can scan the QR code in the expo app to run the app on your phone ^^


*** TO TEST ***
Every page of code should have unit tests 
- yarn test