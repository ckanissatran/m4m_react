import * as React from 'react';
import { render, waitFor  } from '@testing-library/react-native';
import { MyRide } from '../../screens/MyRide';
import MockedNavigator from '../MockedNavigator';

it('should display the inital text for this screen', async () => {
  const { getByText  } = render(
    <MockedNavigator component={MyRide} />
  );
  await waitFor(() => getByText('My Ride!!'));
});