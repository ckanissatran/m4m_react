import * as React from 'react';
import { render, waitFor  } from '@testing-library/react-native';
import { Settings } from '../../screens/Settings';
import MockedNavigator from '../MockedNavigator';

it('should display the inital text for this screen', async () => {
  const { getByText  } = render(
    <MockedNavigator component={Settings} />
  );
  await waitFor(() => getByText('Settingzz'));
});