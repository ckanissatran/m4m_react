import * as React from 'react';
import { render, waitFor  } from '@testing-library/react-native';
import { Home } from '../../screens/Home';
import MockedNavigator from '../MockedNavigator';

it('should display the inital text for this screen', async () => {
  const { getByText  } = render(
    <MockedNavigator component={Home} />
  );
  await waitFor(() => getByText('I am Home'));
});