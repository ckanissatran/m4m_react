import * as React from 'react';
import { render, waitFor  } from '@testing-library/react-native';
import { Profile } from '../../screens/Profile';
import MockedNavigator from '../MockedNavigator';

it('should display the inital text for this screen', async () => {
  const { getByText  } = render(
    <MockedNavigator component={Profile} />
  );
  await waitFor(() => getByText('Profile page'));
});