describe('The M4M App', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('Should Load into the Home Screen', async () => {
    await expect(element(by.text('I am Home'))).toBeVisible();
  });

  it('should show hello screen after tap', async () => {
    await element(by.id('MyRideTab')).tap();
    await expect(element(by.text('My Ride!!'))).toBeVisible();
  });

  it('should show world screen after tap', async () => {
    await element(by.id('SettingsTab')).tap();
    await expect(element(by.text('Settingzz'))).toBeVisible();
  });
});
